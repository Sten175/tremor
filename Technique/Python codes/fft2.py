import numpy as np
from matplotlib import pyplot as plt
from scipy import signal

# Seed the random number generator
np.random.seed(0)
time_step = .01
time_vec = np.arange(0, 70, time_step)
# A signal with a small frequency chirp
sig = np.sin(0.5 * np.pi * time_vec * (1 + .1 * time_vec))
plt.figure(figsize=(8, 5))
plt.plot(time_vec, sig)
# plt.show()

freqs, times, spectrogram = signal.spectrogram(sig)
plt.figure(figsize=(5, 4))
plt.imshow(spectrogram, aspect='auto', cmap='hot_r', origin='lower')
plt.title('Spectrogram')
plt.ylabel('Frequency band')
plt.xlabel('Time window')
plt.tight_layout()


freqs, psd = signal.welch(sig)

plt.figure(figsize=(5, 4))
plt.semilogx(times, freqs)
plt.title('PSD: power spectral density')
plt.xlabel('Frequency')
plt.ylabel('Power')
plt.tight_layout()
plt.show()