import xlrd
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack as fft


acq_temp = 30
Fe = 200
Te = 1/Fe
nb_ech = acq_temp*Fe

t = np.linspace(0, nb_ech*Te, nb_ech)
xf = np.linspace(0, 1/Te, nb_ech)

def plot(k, Nb_ech, Data_axis):
    if k % 3 == 0:
        Axe = str("X")
    elif k % 3 == 1:
        Axe = str("Y")
    else:
        Axe = str("Z")

    yf = fft.fft(Data_axis)

    plt.figure(figsize=(12,6))
    plt.subplot(211)
    plt.title("Évolution temporelle & fréquentielle du capteur " + str(int(k / 3) + 1) + " Axe : " + Axe)
    plt.plot(t,Data_axis,linewidth=1)
    plt.ylabel('Amplitude du signal')
    plt.xlabel('Temps')
    plt.grid(True)
    plt.subplot(212)
    plt.plot(xf, (2 / Nb_ech)*np.abs(yf))
    plt.ylabel('Amplitude de la FFT')
    plt.xlabel('Fréquence')
    axes = plt.gca()
    axes.set_xlim([0, 25])
    plt.grid(True, alpha=1)
    plt.savefig('Images/Analyse-capt-' + str(int(k / 3) + 1) + Axe + '.png')
    plt.close()


workbook = xlrd.open_workbook('Test_30s_Tremblement_reel-2.xlsx', on_demand = True)
worksheet = workbook.sheet_by_index(0)
col1 = [] # The row where we stock the name of the column
col2 = []
col3 = []
for i in range(0,worksheet.nrows):
    col1.append(worksheet.cell_value(i,0))

for i in range(0,worksheet.nrows):
    col2.append(worksheet.cell_value(i,1))

for i in range(0,worksheet.nrows):
    col3.append(worksheet.cell_value(i,2))

vals1 = np.asarray(col1)
vals2 = np.asarray(col2)
vals3 = np.asarray(col3)

plot(0,nb_ech,vals1)
plot(1,nb_ech,vals2)
plot(2,nb_ech,vals3)
