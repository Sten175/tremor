# Created By Nicolas Stenuit
# Version 2.0 = 19/04

import serial
import time
import re
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack

N = 256 # Number of samplepoints
Nf = 128
T = 1.0 / 128.0 # sample spacing

Fe = 128
good = 0
datas = []

t = np.linspace(0.0, N*T, N)
xf = np.linspace(0.0, 1.0/(2.0*T), Nf)
ser = serial.Serial('COM4', 57600, timeout=1)
time.sleep(1)
FFT = []
Somme = 0
Moyenne_FFT = []
Toute_fft = []
Moy_capt = []

print("Démarrage")

print("Veuillez choisir un mode d'utilistation.")
print("A. Pour 30 secondes.")
print("B. Pour 60 secondes.")
print("C. Pour 90 secondes.")
print("D. Pour 120 secondes.")
print("E. Pour 150 secondes.")
print("F. Pour 180 secondes.")
while good == 0:
    choix = input("Quel est votre choix ? : ")
    ser.write(choix.encode())
    if choix == 'A':
        Nb_ligne = 30 * Fe
        Nb_ech = 15
        good = 1
    elif choix == 'B':
        Nb_ligne = 60 * Fe
        Nb_ech = 30
        good = 1
    elif choix == 'C':
        Nb_ligne = 90 * Fe
        Nb_ech = 45
        good = 1
    elif choix == 'D':
        Nb_ligne = 120 * Fe
        Nb_ech = 60
        good = 1
    elif choix == 'E':
        Nb_ligne = 150 * Fe
        Nb_ech = 75
        good = 1
    elif choix == 'F':
        Nb_ligne = 180 * Fe
        Nb_ech = 90
        good = 1
    else:
        print("Veuillez rentrer un choix valide")

for i in range(0, Nb_ligne):
    line = ser.readline()
    if line:
         reg = re.search(r'\'(.*?)\\', str(line))
         data = str(reg.group(1))
         data = data.split(',')
         datas.append(data)
         print(data)


for k in range(0,15):
    for j in range (0,Nb_ech):
        List_ech = [0]
        for i in range(0, 255):
            ech = float(datas[((j*255) + i + 1)][k])
            List_ech.append(ech) # a remplacer par l'echantillon


    for i in range(0, 255):
        Somme = 0
        for j in range(0, Nb_ech):
            Somme = Somme + List_ech[i]
        Moy = Somme / Nb_ech
        List_ech[i] = Moy

    if k % 3 == 0:
        Axe = str("X")
    elif k % 3 == 1:
        Axe = str("Y")
    else:
        Axe = str("Z")



    yf = scipy.fftpack.fft(List_ech)
    plt.subplot(211)
    plt.title("Évolution temporelle & fréquentielle du capteur "+ str(int(k/3)+1) + " Axe : " + Axe)
    plt.plot(t,List_ech)
    plt.ylabel('Amplitude du signal')
    plt.xlabel('Temps')
    plt.subplot(212)
    plt.plot(xf, 2.0 / N * np.abs(yf[:N // 2]))
    plt.ylabel('Amplitude de la FFT')
    plt.xlabel('Fréquence')
    plt.savefig('Images/Analyse-capt-' +str(int(k/3)+1) + Axe + '.png')
    plt.close()















































