import matplotlib.pyplot as plt
import librosa
import librosa.display
import IPython.display as ipd

x, sr = librosa.load('diapason.wav', sr=44100)
# print(type(x), type(sr))
print(x.shape, sr)

plt.figure(figsize=(12, 5))
librosa.display.waveplot(x, sr=sr)


X = librosa.stft(x)
Xdb = librosa.amplitude_to_db(abs(X))
plt.figure(figsize=(12, 5))
librosa.display.specshow(Xdb, sr=sr, x_axis='time', y_axis='hz')
plt.colorbar()
plt.show()

# plt.figure(figsize=(14, 5))
# librosa.display.specshow(Xdb, sr=sr, x_axis='time', y_axis='log')
# plt.colorbar()
# plt.show()