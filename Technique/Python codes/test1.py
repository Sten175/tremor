from scipy.special import cbrt, exp10
import numpy as np
from matplotlib import pyplot as plt

f = 1  # Frequency
f_s = 210  # Sampling rate
t = np.linspace(0, 2, 2 * f_s, endpoint=False)
x = -0.0005*42*np.pi*42*np.pi*np.sin(f * 2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, x)
ax.set_xlabel('Time [s]')
ax.set_ylabel('Signal amplitude')
plt.show()