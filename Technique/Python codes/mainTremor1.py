# Created By Nicolas Stenuit
# Reviwed By Lucas Garcia
# Version 2.2 = 13/05
import serial
from tkinter import *
import re
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack
import random
import math
from fpdf import FPDF


# Variables à revoir
# = 256 #Number of samplepoints

Nf = 200 #valeurs pour la FFT
T = 1.0 / 200 #sample spacing
Fe = 200

datas = []
#ser = serial.Serial('COM4', 57600, timeout=1)
firstclick = True

def rapport_gen(nom, prenom, n_date):
    nom_ln = str("Nom : " + nom)
    prenom_ln = str("Prenom : " + prenom)
    ndate = n_date.format('%d/%m/%Y')
    ndate_ln = str("Date de naissance : " + ndate)

    # Config PDF generator
    pdf = FPDF(orientation='P', unit='mm', format='A4')
    pdf.add_page()
    pdf.set_font("Arial", size=12)

    # Textes
    pdf.cell(30,6, txt=nom_ln, ln=1, align="L")
    pdf.cell(30,6, txt=prenom_ln, ln=2, align="L")
    pdf.cell(30,6, txt=ndate_ln, ln=3, align="L")

    #Images
    # Capt 1
    pdf.image('Images/Analyse-capt-1X.png', x=0, y=30, w=200)
    pdf.image('Images/Analyse-capt-1Y.png', x=0, y=110, w=200)
    pdf.image('Images/Analyse-capt-1Z.png', x=0, y=190, w=200)

    pdf.output("Rapport_Patient.pdf")

def define_value(choix):
    good = 0

    while good == 0:
#        ser.write(choix.encode())
        if choix == 'A':
            Nb_ligne = 30 * Fe
            Nb_ech = 15
            good = 1
        elif choix == 'B':
            Nb_ligne = 60 * Fe
            Nb_ech = 30
            good = 1
        elif choix == 'C':
            Nb_ligne = 90 * Fe
            Nb_ech = 45
            good = 1
        elif choix == 'D':
            Nb_ligne = 120 * Fe
            Nb_ech = 60
            good = 1
        elif choix == 'E':
            Nb_ligne = 150 * Fe
            Nb_ech = 75
            good = 1
        elif choix == 'F':
            Nb_ligne = 180 * Fe
            Nb_ech = 90
            good = 1
        else:
            print("Veuillez rentrer un choix valide")
    return Nb_ligne, Nb_ech
#Nb_ligne = secondes * FE = Nombre total de valeur finale

#def read_serial(Nb_ligne):
#    for i in range(0, Nb_ligne):
#        line = ser.readline()
#        if line:
#            reg = re.search(r'\'(.*?)\\', str(line))
#            data = str(reg.group(1))
#            data = data.split(',')
#            datas.append(data)
#            print(i, data)
#    return datas

def get_axis_value(Axe,data_List,Nb_ligne,):
    List = []
    for i in range(0,Nb_ligne):
        #print(data_List[i][Axe], i,Axe)
        #ech =float(data_List[i][Axe])
        ech = np.sin(5*((i/Fe))*2.0*np.pi)
        #ech = random.randint(0,100)
        List.append(ech)
    return List

# Moyenne -> à essayer sans ou de le changer
#def mean(Nb_ech,List):
#    Somme = 0.0
#    for i in range(0, Nb_ech):
#        Somme = Somme + List[256+i]
#    Moy = Somme / Nb_ech
#    print(Moy)
#    return Moy

def plot(k,List_ech,Nb_ligne,Data_axis):
    if k % 3 == 0:
        Axe = str("X")
    elif k % 3 == 1:
        Axe = str("Y")
    else:
        Axe = str("Z")

    t = np.linspace(0.0, Nb_ligne * T, Nb_ligne) # jusque 30 par pas de 3840 -> 128 valeurs par seconde
    xf = np.linspace(0.0, 1.0 / T, Nb_ligne)  #
    yf = scipy.fftpack.fft(Data_axis)
    plt.figure(figsize=(12,4.8))
    plt.subplot(211)
    plt.title("Évolution temporelle & fréquentielle du capteur " + str(int(k / 3) + 1) + " Axe : " + Axe)
    plt.plot(t,Data_axis,linewidth=0.1)
    plt.ylabel('Amplitude du signal')
    plt.xlabel('Temps')
    plt.subplot(212)
    plt.plot(xf, (2/Nb_ligne) * np.abs(yf))

    plt.ylabel('Amplitude de la FFT')
    plt.xlabel('Fréquence')
    axes = plt.gca()
    axes.set_xlim([0, 25])
    plt.grid(True)
    plt.savefig('Images/Analyse-capt-' + str(int(k / 3) + 1) + Axe + '.png')
    plt.close()

def affiche():
    arg = var_case.get()
    if arg == 0:
        choix = "A"
    elif arg == 1:
        choix = "B"
    elif arg == 2:
        choix = "C"
    elif arg == 3:
        choix = "D"
    elif arg == 4:
        choix = "E"
    elif arg == 5:
        choix = "F"
    print(choix)
    return choix

def start():
    choix = affiche()
    Nb_ligne, Nb_ech = define_value(choix)
    print(Nb_ech)
    print(Nb_ligne)
    #Datas = read_serial(Nb_ligne)
    Datas = []
    print(var_prenom.get())
    Moyenne = []
    for k in range(0,3):
        Data_Axis = get_axis_value(k,Datas,Nb_ligne)
        # for j in range (0,256):
            # Moyenne.append(mean(Nb_ech,Data_Axis))

        plot(k,Moyenne,Nb_ligne,Data_Axis)



def rap_gen():
    rapport_gen(var_nom.get(), var_prenom.get(), var_date.get())

def on_entry_click(event):
    """function that gets called whenever entry1 is clicked"""
    global firstclick

    if firstclick: # if this is the first time they clicked it
        firstclick = False
        entry1.delete(0, "end") # delete all the text in the entry
        entry2.delete(0, "end")
        entry3.delete(0, "end")


fenetre = Tk()

var_nom = StringVar()
var_prenom = StringVar()
var_date = StringVar()

fenetre.title("Tremor Measurement")
fenetre.iconbitmap("logo0.ico")

champ_label = Label(fenetre, text="Coded By Tremor Measurement : Work in progress !")
var_case = IntVar()
cb = []

label = Label(fenetre, text="Nom du patient: ")
label.grid(row=0, column=0, sticky='w')

entry1 = Entry(fenetre, bd=1, width=33, textvariable=var_nom)
entry1.insert(0, 'Veuillez rentrer le nom du patient:')
entry1.bind('<FocusIn>', on_entry_click)
entry1.grid(row=0, column=1, sticky='w')

label = Label(fenetre, text="Prénom du patient: ")
label.grid(row=1, column=0, sticky='w')

entry2 = Entry(fenetre, bd=1, width=33, textvariable=var_prenom)
entry2.insert(0, 'Veuillez rentrer le prénom du patient:')
entry2.bind('<FocusIn>', on_entry_click)
entry2.grid(row=1, column=1, sticky='w')

label = Label(fenetre, text="Date de naissance du patient:")
label.grid(row=2, column=0, sticky='w')

entry3 = Entry(fenetre, bd=1, width=13, textvariable=var_date)
entry3.insert(0, 'DD/MM/YYYY')
entry3.bind('<FocusIn>', on_entry_click)
entry3.grid(row=2, column=1, sticky='w')

for i in range(6):
    cb.append(Checkbutton(fenetre, text=str((i + 1) * 30) + " Secondes", onvalue=i, variable=var_case))
    # Creating and adding checkbutton to list
    cb[i].grid(row=i, column=2)

bouton_start = Button(fenetre, text="Démarrer", command=start)
bouton_start.grid(row=5, column=0, sticky='e')
bouton_stop = Button(fenetre, text="Annuler", command=fenetre.quit)
bouton_stop.grid(row=5, column=1, sticky='w', padx=17)
bouton_print = Button(fenetre, text="Imprimer le rapport", command=rap_gen)
bouton_print.grid(row=5, column=2, sticky='w', padx=15)
# On affiche le label dans la fenêtre
champ_label.grid(row=17, column=1, sticky='s', ipady=5)
# On démarre la boucle Tkinter qui s'interompt quand on ferme la fenêtre
fenetre.mainloop()
##############