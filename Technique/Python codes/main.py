# Created By Nicolas Stenuit
# Version 2.1 = 20/04
import serial
from tkinter import *
import re
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack
import random
N = 256 # Number of samplepoints
Nf = 128
T = 1.0 / 128.0 # sample spacing
Fe = 128
datas = []
ser = serial.Serial('COM4', 57600, timeout=1)
firstclick = True
def define_value(choix):
    good = 0

    while good == 0:
        ser.write(choix.encode())
        if choix == 'A':
            Nb_ligne = 30 * Fe
            Nb_ech = 15
            good = 1
        elif choix == 'B':
            Nb_ligne = 60 * Fe
            Nb_ech = 30
            good = 1
        elif choix == 'C':
            Nb_ligne = 90 * Fe
            Nb_ech = 45
            good = 1
        elif choix == 'D':
            Nb_ligne = 120 * Fe
            Nb_ech = 60
            good = 1
        elif choix == 'E':
            Nb_ligne = 150 * Fe
            Nb_ech = 75
            good = 1
        elif choix == 'F':
            Nb_ligne = 180 * Fe
            Nb_ech = 90
            good = 1
        else:



            print("Veuillez rentrer un choix valide")
    return Nb_ligne, Nb_ech
def read_serial(Nb_ligne):
    for i in range(0, Nb_ligne):
        line = ser.readline()
        if line:
            reg = re.search(r'\'(.*?)\\', str(line))
            data = str(reg.group(1))
            data = data.split(',')
            datas.append(data)
            print(i, data)
    return datas
def get_axis_value(Axe,data_List,Nb_ligne,):
    List = []
    for i in range(0,Nb_ligne):
        print(data_List[i][Axe], i,Axe)
        #ech =float(data_List[i][Axe])
        ech = random.randint(0,100)
        List.append(ech)
    return List
def mean(Nb_ech,List):
    Somme = 0.0
    for i in range(0, Nb_ech):
        Somme = Somme + List[256+i]
    Moy = Somme / Nb_ech
    print(Moy)
    return Moy
def plot(k,List_ech,Nb_ligne,Data_axis):
    if k % 3 == 0:
        Axe = str("X")
    elif k % 3 == 1:
        Axe = str("Y")
    else:
        Axe = str("Z")

    t = np.linspace(0.0, Nb_ligne * T, Nb_ligne)
    xf = np.linspace(0.0, 1.0 / (2.0 * T), Nf)
    yf = scipy.fftpack.fft(List_ech)
    plt.figure(figsize=(12,4.8))
    plt.subplot(211)
    plt.title("Évolution temporelle & fréquentielle du capteur " + str(int(k / 3) + 1) + " Axe : " + Axe)
    plt.plot(t,Data_axis,linewidth=0.1)
    plt.ylabel('Amplitude du signal')
    plt.xlabel('Temps')
    plt.subplot(212)
    plt.plot(xf, 2.0 / N * np.abs(yf[:N // 2]))
    plt.ylabel('Amplitude de la FFT')
    plt.xlabel('Fréquence')
    plt.savefig('Images/Analyse-capt-' + str(int(k / 3) + 1) + Axe + '.png')
    plt.close()
def affiche():
    arg = var_case.get()
    if arg == 0:
        choix = "A"
    elif arg == 1:
        choix = "B"
    elif arg == 2:
        choix = "C"
    elif arg == 3:
        choix = "D"
    elif arg == 4:
        choix = "E"
    elif arg == 5:
        choix = "F"
    print(choix)
    return choix
def start():
    choix = affiche()
    Nb_ligne, Nb_ech = define_value(choix)
    print(Nb_ech)
    print(Nb_ligne)
    Datas = read_serial(Nb_ligne)
    Moyenne = []
    for k in range(0,15):
        Data_Axis = get_axis_value(k,Datas,Nb_ligne)
        for j in range (0,256):
            Moyenne.append(mean(Nb_ech,Data_Axis))
        plot(k,Moyenne,Nb_ligne,Data_Axis)
def on_entry_click(event):
    """function that gets called whenever entry1 is clicked"""
    global firstclick

    if firstclick: # if this is the first time they clicked it
        firstclick = False
        entry.delete(0, "end") # delete all the text in the entry
fenetre = Tk()

fenetre.title("Tremor Measurement")
fenetre.iconbitmap("logo2.ico")

champ_label = Label(fenetre, text="Coded By Tremor Measurement : Work in progress !")
var_case = IntVar()
cb = []

label = Label(fenetre, text="Nom du patient: ")
label.grid(row=0, column=0, sticky='w')

entry = Entry(fenetre, bd=1, width=33)
entry.insert(0, 'Veuillez rentrer le nom du patient:')
entry.bind('<FocusIn>', on_entry_click)
entry.grid(row=0, column=1, sticky='w')

label = Label(fenetre, text="Prénom du patient: ")
label.grid(row=1, column=0, sticky='w')

entry = Entry(fenetre, bd=1, width=33)
entry.insert(0, 'Veuillez rentrer le prénom du patient:')
entry.bind('<FocusIn>', on_entry_click)
entry.grid(row=1, column=1, sticky='w')

label = Label(fenetre, text="Date de naissance du patient: ")
label.grid(row=2, column=0, sticky='w')

entry = Entry(fenetre, bd=1, width=13)
entry.insert(0, 'DD/MM/YYYY')
entry.bind('<FocusIn>', on_entry_click)
entry.grid(row=2, column=1, sticky='w')

for i in range(6):
    cb.append(Checkbutton(fenetre, text=str((i + 1) * 30) + " Secondes", onvalue=i, variable=var_case))
    # Creating and adding checkbutton to list
    cb[i].grid(row=i, column=2)

bouton_start = Button(fenetre, text="Start Analysis", command=start)
bouton_start.grid(row=5, column=0, sticky='e')
bouton_stop = Button(fenetre, text="Quitter", command=fenetre.quit)
bouton_stop.grid(row=5, column=1, sticky='w', padx=17)
# On affiche le label dans la fenêtre
champ_label.grid(row=17, column=1, sticky='s', ipady=5)
# On démarre la boucle Tkinter qui s'interompt quand on ferme la fenêtre
fenetre.mainloop()
##############