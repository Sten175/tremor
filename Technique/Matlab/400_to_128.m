% Initialisation
clear all; close all; clc;

% Filtre
p = tf([1 0],1);
Filtre = 1/(((p/(2*pi*99))+1)*((p/(2*pi*99))+1));

% Bode
set(cstprefs.tbxprefs,'FrequencyUnits','Hz');
figure;
bode(Filtre);

%Temps d'échantillonage
Fe_Acc = 400;
Te_Acc = 1/Fe_Acc;

%Transformée en Z
Filtre_Z=c2d(Filtre,Te_Acc,'zoh');
%C_Z=c2d(Filtre,Te,'tustin')


%Temps d'échantillonage
Fe_Ard = 128;
Te_Ard = 1/Fe_Ard;

%Transformée en Z
Data_Ard=c2d(Filtre,Te_Ard,'zoh');
%C_Z=c2d(Filtre,Te,'tustin')


tc = [0:0.0001:0.2]
uc = sin(21*2*pi*tc)
[F,tc] = lsim(Filtre,uc,tc)
t_Acc = [0:Te_Acc:0.2]
u_Acc = sin(21*2*pi*t_Acc)
[F_400,t_Acc] = lsim(Filtre_Z,u_Acc,t_Acc)
t_Ard = [0:Te_Ard:0.2]
u_Ard = sin(21*2*pi*t_Ard)
[F_128,t_Ard] = lsim(Data_Ard,u_Ard,t_Ard);

figure;
plot(tc, uc, tc, F, t_Acc, u_Acc)
hold on
stairs(t_Acc, F_400)
hold on
stairs(t_Ard, F_128)
hold off


