% Initialisation
clear all; close all; clc;

% Filtre
p = tf([1 0],1);
Filtre = 1/(((p/(2*pi*100))+1)*((p/(2*pi*100))+1));

% Bode
set(cstprefs.tbxprefs,'FrequencyUnits','Hz');
figure;
bode(Filtre);

%Temps d'échantillonage
Fe = 200;
Te = 1/Fe;


tc = [0:0.0001:0.2]
uc = sin(21*2*pi*tc)
%t = [0:Te:0.2]
%u = sin(21*2*pi*t)
[Fc,tc] = lsim(Filtre,uc,tc);
%[F_Z,t] = lsim(Filtre_Z,u,t);

figure;
plot(tc, uc)
hold on
plot(tc,Fc)
hold off



%----------------------------------

% %Transformée en Z
% Filtre_Z=c2d(Filtre,Te,'zoh');
% %C_Z=c2d(Filtre,Te,'tustin')
% 
% figure;
% bode(Filtre_Z);
% 
% figure;
% step(Filtre, Filtre_Z);
