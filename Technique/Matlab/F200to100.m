% Initialisation
clear all; close all; clc;

% Filtre
p = tf([1 0],1);
Filtre = 1/(((p/(2*pi*99))+1)*((p/(2*pi*99))+1));

% Bode
set(cstprefs.tbxprefs,'FrequencyUnits','Hz');
figure;
bode(Filtre);

%Temps d'�chantillonage
Fe = 200;
Te = 1/Fe;

%Temps d'�chantillonnage fin pour estimaion r�elle
Tr = 0.0001;
Fr = 1/Tr;

tc = [0:Tr:(0.2-Tr)]
uc = 100*sin(21*2*pi*tc)         % Signal r�el
t = [0:Te:(0.2-Te)]  
u = 100*sin(21*2*pi*t)   
[F,tc] = lsim(Filtre,uc,tc); % Signal filtr� 
[F_0,t] = lsim(Filtre,u,t);  % Signal �chantillonn�

figure;
plot(tc, uc)
hold on
plot(tc,F)
hold on
plot(t,F_0)
title('Traitement d un signal de 21Hz')
xlabel('Temps (s)')
ylabel('Amplitude (%)')
legend('Signal r�el','Signal filtr�', 'Signal �chantillon�')
hold off


%Signal continue-----------------------------------------------
Y = fft(uc);

L = length(tc);
P2 = abs(Y/L);
P1 = P2(1:((L/2)+1));
P1(2:end-1) = 2*P1(2:end-1);

f = Fr*(0:(L/2))/L;

figure;
plot(f(1:21),P1(1:21)) 
hold on

%Signal filtr�-----------------------------------------------
Y = fft(F);

L = length(tc);
P2 = abs(Y/L);
P1 = P2(1:((L/2)+1));
P1(2:end-1) = 2*P1(2:end-1);

f = Fr*(0:(L/2))/L;

%figure;
plot(f(1:21),P1(1:21)) 
hold on

%Signal �chantillonn�-----------------------------------------------
Y = fft(F_0);

L = length(t);
P2 = abs(Y/L);
P1 = P2(1:((L/2)+1));
P1(2:end-1) = 2*P1(2:end-1);

f = Fe*(0:(L/2))/L;

%figure;
plot(f,P1) 
title('Spectral analysis')
xlabel('Frequency (Hz)')
ylabel('Amplitude (%)')
legend('Real signal','Filtred signal', 'Sampled signal')
hold off